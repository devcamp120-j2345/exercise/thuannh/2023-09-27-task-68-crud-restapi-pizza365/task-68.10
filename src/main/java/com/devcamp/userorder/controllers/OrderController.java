package com.devcamp.userorder.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.userorder.model.COrder;
import com.devcamp.userorder.model.CUser;
import com.devcamp.userorder.repository.IOrderRepository;
import com.devcamp.userorder.repository.IUserRepository;
import com.devcamp.userorder.service.UserService;
import com.devcamp.userorder.service.OrderService;

@RestController
@CrossOrigin
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private UserService userService;
    @Autowired
    private OrderService orderService;
    //get all order list
    @GetMapping("/all")
    public ResponseEntity<List<COrder>> getAllOrdersApi(){
        try {
            return new ResponseEntity<>(orderService.getAllOrders(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    //get order by user id
    @GetMapping("/search/userId={id}")
    public ResponseEntity<Set<COrder>> getOrderByuserIdApi(@PathVariable(name="id")long id){
        try {
            Set<COrder> userOrders = userService.getOrderByUserId(id);
            if (userOrders != null ){
                return new ResponseEntity<>(userOrders, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //get order detail by id
    @Autowired
    IOrderRepository orderRepository;
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable("id") long id){
        COrder order = orderRepository.findById(id);
        if (order != null){
            return new ResponseEntity<>(order, HttpStatus.OK);
        } else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    //create new order with user id
    @Autowired
    IUserRepository userRepository;
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createUser(@PathVariable("id") Long id, @RequestBody COrder pOrder){
        try {
            Optional<CUser> userData = userRepository.findById(id);
            if (userData.isPresent()){
                pOrder.setCreated(new Date());
                pOrder.setUpdated(null);
                pOrder.setUser(userData.get());
                COrder _order = orderRepository.save(pOrder);
                return new ResponseEntity<>(_order, HttpStatus.CREATED);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //update order
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable(name="id")long id, @RequestBody COrder pOrderUpdate){
        try {
            COrder order = orderRepository.findById(id);
            if (order != null){
                order.setOrderCode(pOrderUpdate.getOrderCode());
                order.setPizzaSize(pOrderUpdate.getPizzaSize());
                order.setPizzaType(pOrderUpdate.getPizzaType());
                order.setPrice(pOrderUpdate.getPrice());
                order.setVoucherCode(pOrderUpdate.getVoucherCode());
                order.setPaid(pOrderUpdate.getPaid());
                order.setUpdated(new Date());

                return new ResponseEntity<>(orderRepository.save(order), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete order by id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<COrder> deleteOrderById(@PathVariable("id") long id) {
        try {
            orderRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
